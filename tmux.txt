cat .tmux.comf

set -g default-terminal "screen-256color"

set -g status-bg red
set -g status-fg black
set -g mouse on


set -g status-justify left               # Aligne les titres de fenetres a gauche
set -g status-bg colour1                 # Status bar noir sur rouge
set -g status-fg colour0                 #
set -g status-interval 2                 # Evite des bug de refraichissement

setw -g window-status-current-fg colour1 # Inversion des couleur pour l'onglet selectione
setw -g window-status-current-bg colour0 #
set-option -g history-limit 50000        # Permet de scroller 10k lignes
set -g history-limit 10000               #
[cp.olaso@vmansiblepr01 ~]$

