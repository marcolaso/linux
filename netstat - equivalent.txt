Command ss 

    ss -t

    ss -A tcp
    State   Recv-Q   Send-Q Local Address:Port      Peer Address:Port
    ESTAB   0       0       10.252.107.130:ssh      10.20.21.201:46723
    ESTAB   0       0       10.252.107.130:ssh      172.20.61.41:55284
    ESTAB   12      0       10.252.107.130:53656    10.20.20.190:5044
   

    ss -ltn
    State      Recv-Q         Send-Q             Local Address:Port            Peer Address:Port
    LISTEN     0              128                127.0.0.53%lo:53              0.0.0.0:*
    LISTEN     0              128                0.0.0.0:22                    0.0.0.0:*
    LISTEN     0              50                 0.0.0.0:42879                 0.0.0.0:*
    LISTEN     0              5                  0.0.0.0:3010                  0.0.0.0:*
    LISTEN     0              128                0.0.0.0:10050                 0.0.0.0:*


